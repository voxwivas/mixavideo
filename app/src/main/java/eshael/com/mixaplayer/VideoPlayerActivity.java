package eshael.com.mixaplayer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.UnsupportedEncodingException;

import eshael.com.mixaplayer.util.ApiClient;

/**
 * Created by Raphael on 7/7/2016.
 */
public class VideoPlayerActivity extends AppCompatActivity {

    private VideoView mainPlayer;
    private WebView adview;
    private FrameLayout adFrame;
    private Button adSkip;
    private LinearLayout linearLayout;
    private String vidAddress ="https://ia800201.us.archive.org/22/items/ksnn_compilation_master_the_internet/ksnn_compilation_master_the_internet_512kb.mp4";
    private MediaController vidControl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_videoplayer);

        mainPlayer = (VideoView)findViewById(R.id.VideoView);
        adview = (WebView)findViewById(R.id.AdsView);
        adFrame = (FrameLayout)findViewById(R.id.adsFrame);
        adSkip = (Button)findViewById(R.id.adSkip);
        linearLayout = (LinearLayout)findViewById(R.id.ControlLayout);

        String url =getIntent().getStringExtra("url");
        Uri vidUri=null;

        String encodedURL;
        try
        {
            encodedURL = java.net.URLEncoder.encode(url,"UTF-8");
            vidUri = Uri.parse(ApiClient.BASE_URL+"video/"+encodedURL);
            Log.d("Vid URl ", vidUri+"");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        adview.getSettings().setJavaScriptEnabled(true);
        adview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        adview.getSettings().setAppCacheEnabled(true);
        adview.getSettings().setLoadWithOverviewMode(true);
        adview.setBackgroundColor(Color.BLACK);

        adview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {

                String javascript = "javascript:document.querySelectorAll(\""+".mejs-ted1\""+")[0].style.width = window.innerWidth+\""+"px\""+";document.querySelectorAll(\""+".mejs-ted1\""+")[0].style.height = window.innerHeight+\""+"px\""+";document.querySelectorAll(\""+".mejs-ted1\""+")[1].style.width = window.innerWidth+\""+"px\""+";document.querySelectorAll(\""+".mejs-ted1\""+")[1].style.height = window.innerHeight+\""+"px\";";
                Log.d("JS",javascript);
//                view.loadUrl(javascript);
            }
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url != null && url.startsWith("http://")) {
                    view.getContext().startActivity( new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    adview.loadUrl(url);
                    return true;
                } else {
                    return false;
                }
            }
        });

        refreshWebView(ApiClient.BASE_URL+"ad.html");

        DisplayMetrics metrics = new DisplayMetrics(); getWindowManager().getDefaultDisplay().getMetrics(metrics);
        android.widget.FrameLayout.LayoutParams params = (android.widget.FrameLayout.LayoutParams) mainPlayer.getLayoutParams();
        params.width =  metrics.widthPixels;
        params.height = metrics.heightPixels;
        params.leftMargin = 0;

        mainPlayer.setLayoutParams(params);

        mainPlayer.setVideoURI(vidUri);
        vidControl = new MediaController(this);
        vidControl.setAnchorView(mainPlayer);

        mainPlayer.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                Toast.makeText(VideoPlayerActivity.this,"Main Player",Toast.LENGTH_LONG).show();
//                mainPlayer.start();
                return false;
            }
        });
        adSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adFrame.setVisibility(View.INVISIBLE);
                adview.destroy();
                mainPlayer.start();
                mainPlayer.setMediaController(vidControl);
            }
        });
    }


    private void refreshWebView(String url)
    {
       adview.loadUrl(url);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            if (hasFocus) {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            }
        }
    }
}
