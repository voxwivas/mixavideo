package eshael.com.mixaplayer.util;

import java.util.List;

import eshael.com.mixaplayer.model.Category;
import eshael.com.mixaplayer.model.Video;
import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Raphael on 7/11/2016.
 */
public interface ApiInterface {

    @GET("api")
    Call<List<Video>> getVideos(@Query("topic") String apiKey);

    @GET("api")
    Call<List<Category>> getCategories(@Query("topic") String category);

    @GET("api")
    Call<List<Video>> getCategoryVideos(@Query("topic") String category,@Query("name")String categoryName);

}
