package eshael.com.mixaplayer.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import eshael.com.mixaplayer.R;

/**
 * Created by Raphael on 7/7/2016.
 */
public class VideoDetailFragment extends Fragment
{
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public VideoDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.video_detail, container, false);

        Bundle bundle = getArguments();
        ((TextView) rootView.findViewById(R.id.video_detail)).setText(bundle.getString("desc"));

        return rootView;
    }
}
