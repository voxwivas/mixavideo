package eshael.com.mixaplayer.model;

import com.orm.SugarRecord;

/**
 * Created by Raphael on 7/7/2016.
 */
public class Category extends SugarRecord {

    private int _id;
    private String name;

    public Category() {
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
