package eshael.com.mixaplayer.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import java.util.List;

import eshael.com.mixaplayer.R;
import eshael.com.mixaplayer.model.Video;
import eshael.com.mixaplayer.util.ApiClient;

/**
 * Created by Raphael on 7/7/2016.
 */
public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.VideoViewHolder> {

    private List<Video> videos;
    private int rowLayout;
    private Context context;

    public  class VideoViewHolder extends RecyclerView.ViewHolder {
        TextView movieTitle;
        TextView category;
        ImageView thumbnail;
        TextView duration;

        public VideoViewHolder(View v)
        {
            super(v);
            movieTitle = (TextView) v.findViewById(R.id.vid_title);
            thumbnail = (ImageView)v.findViewById(R.id.thumbnail);
            duration  = (TextView)v.findViewById(R.id.duration);
            category  = (TextView)v.findViewById(R.id.vid_category);
        }
    }

    public VideoAdapter(List<Video> videos, int rowLayout, Context context) {
        this.videos = videos;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public VideoAdapter.VideoViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new VideoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VideoViewHolder holder, final int position) {
        holder.movieTitle.setText(videos.get(position).getTitle());
        holder.duration.setText(videos.get(position).getDuration());
        holder.category.setText(videos.get(position).getCategory());
        Picasso.with(context).load(ApiClient.BASE_URL+"images/"+videos.get(position).getThumbnail()).into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return videos.size();
    }

}
