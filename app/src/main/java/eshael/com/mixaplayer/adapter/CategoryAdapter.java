package eshael.com.mixaplayer.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;

import java.util.ArrayList;
import java.util.List;

import eshael.com.mixaplayer.R;
import eshael.com.mixaplayer.VideoDetailActivity;
import eshael.com.mixaplayer.model.Category;

/**
 * Created by Raphael on 7/7/2016.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>{
    private Context context;
    private List<Category> categoryList;
    private ColorGenerator generator = ColorGenerator.MATERIAL;
    private int rowLayout;


    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView thumbnail;

        public CategoryViewHolder(View v)
        {
            super(v);
            thumbnail = (ImageView)v.findViewById(R.id.category_letter);
            name  = (TextView)v.findViewById(R.id.category_title);
        }
    }

    public CategoryAdapter(List<Category> categories, int rowLayout, Context context) {
        this.categoryList = categories;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public CategoryAdapter.CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, final int position) {
        holder.name.setText(categoryList.get(position).getName());
        String letter = String.valueOf(categoryList.get(position).getName().charAt(0));
        TextDrawable drawable = TextDrawable.builder().buildRect(letter, generator.getRandomColor());
        holder.thumbnail.setImageDrawable(drawable);
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }
}
