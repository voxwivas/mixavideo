package eshael.com.mixaplayer;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;

import java.io.UnsupportedEncodingException;

import eshael.com.mixaplayer.util.ApiClient;

public class WebPlayer extends AppCompatActivity {

    WebView htmlViewer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.content_web_player);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);


        htmlViewer = (WebView) findViewById(R.id.player);
        htmlViewer.getSettings().setJavaScriptEnabled(true);
        htmlViewer.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        htmlViewer.getSettings().setAppCacheEnabled(true);
        htmlViewer.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
//                String javascript = "javascript:document.querySelector(\"body > a > img\").setAttribute(\"width\",\"100%\");document.querySelector(\"body > a > img\").setAttribute(\"height\",\"190\");\n";
//                view.loadUrl(javascript);
            }
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url != null && url.startsWith("http://")) {
//                    view.getContext().startActivity( new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                    htmlViewer.loadUrl(url);
                    return true;
                } else {
                    return false;
                }
            }
        });

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT, RelativeLayout.LayoutParams.FILL_PARENT);
        htmlViewer.setLayoutParams(params);


        refreshWebView(getIntent().getStringExtra("title"),getIntent().getStringExtra("desc"),getIntent().getStringExtra("thumb"),getIntent().getStringExtra("url"));
    }

    private void refreshWebView(String title,String info,String thumbnail,String url)
    {
//        http://localhost:8080/video/api?topic=player&title="Ttest"&url="http://localhost:8080/video/video/August Alsina - No Love ft. Nicki Minaj.mp4"&thumbnail=""&info="test decsription"

        Log.d("URl",ApiClient.BASE_URL+url);
        htmlViewer.loadUrl(ApiClient.BASE_URL+"api?topic=player&title=\""+title+"\"&url=\""+ApiClient.BASE_URL+"/video/"+url+"\"&thumbnail=\""+thumbnail+"\"&info=\""+info+"\"");
  }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
            if (hasFocus) {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            }
        }
    }
}
